# Fiyo Fullstack Test

This test is to get a general impression of your capabilties in both frontend and backend. 
You are free to use the techniques you prefer. Please spend a maximum of one hour on frontend and a maximum of one hour on backend functionality.

### Goal
Create an application which represents an elevator, which at least includes the following functions:
- Multiple people (devices/browsers) can use the same elevator (which lives in the backend);
- When someone enters the elevator and pushes a button, the elevator moves to another floor;
- When someone pushes the button on another floor, the elevator reacts;

Graphically there is no need to make it perfect, as long as it's clear it's an elevator.
The visitor should be able to use the buttons and see the elevator move.

**Bonus points**:
Make it easy to add extra elevators on both frontend as backend

_You're free to implement it the way you like (except by using elevator scripts, created by someone else)_